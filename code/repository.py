BASE_LABELS = [
    Label(title='bug', color='#F56C6C'),
    Label(title='feature', color='#67C23A'),
]

class Repository:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.owner_id = kwargs.get('owner_id', None)
        self.name = kwargs.get('name', None)
        self.description = kwargs.get('description', None)
        self.num_issues = kwargs.get('num_issues', None)
        self.is_private = kwargs.get('is_private', None)
        self.created_at = kwargs.get('created_at', None)
        self.updated_at = kwargs.get('updated_at', None)

        self.labels = BASE_LABELS
        self.owner = None
        self.contributors = []

    def add_contributor(self, user_id: int, commit=True) -> None:
        insert_query = '''
        INSERT INTO repo_access (user_id, repo_id) VALUES (%s, %s);
        '''
        with g.db.cursor() as cursor:
            cursor.execute(insert_query, (user_id, self.id))
            if commit:
                g.db.commit()
        contributor = User.get_by_id(user_id)
        self.contributors.append(contributor)

    def delete(self) -> None:
        delete_query = '''
        DELETE FROM repository WHERE id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(delete_query, (self.id,))

    def get_contributors(self) -> List['User']:
        select_query = '''
        SELECT t_user.* FROM t_user
            JOIN repo_access ra on t_user.id = ra.user_id
            WHERE ra.repo_id = %s;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(select_query, (self.id,))
            db_rows = cursor.fetchall()
            return [
                User(**db_row)
                for db_row in db_rows
            ] if db_rows is not None else []

    def replace_contributors(self, new_ids: List[int]) -> None:
        current_ids = [
            contributor.id
            for contributor in self.contributors
        ]
        if current_ids == new_ids:
            return
        delete_query = 'DELETE FROM repo_access WHERE repo_id = %s;'
        with g.db:
            with g.db.cursor() as cursor:
                cursor.execute(delete_query, (self.id,))
                g.db.commit()
                self.contributors = []
            for contributor_id in new_ids:
                self.add_contributor(contributor_id)

    def merge(self, patch_data: dict) -> 'Repository':
        if 'name' in patch_data:
            self.name = patch_data['name']
        if 'description' in patch_data:
            self.description = patch_data['description']
        if 'is_private' in patch_data:
            self.is_private = patch_data['is_private']

        return self

    def save(self, commit=True) -> None:
        insert_query = '''
        INSERT INTO repository
        (owner_id, name, description, is_private)
        VALUES (%s, %s, %s, %s)
        RETURNING *;
        '''
        update_query = '''
        UPDATE repository
            SET owner_id = %s,
                name = %s,
                description = %s,
                is_private = %s
            WHERE id = %s;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            if self.id is not None:
                cursor.execute(
                    update_query,
                    (
                        self.owner_id,
                        self.name,
                        self.description,
                        self.is_private,
                        self.id
                    )
                )
            else:
                repo_name = self.name.lower()
                cursor.execute(
                    insert_query,
                    (self.owner_id, repo_name,
                        self.description, self.is_private)
                )
                db_row = cursor.fetchone()
                self.id = db_row['id']
                self.num_issues = db_row['num_issues']
                self.created_at = db_row['created_at']
                self.updated_at = db_row['updated_at']

                for label in self.labels:
                    label.repo_id = self.id
                    label.save()
            if commit:
                g.db.commit()

    @staticmethod
    def from_db_row(data: tuple) -> 'Repository':
        repository = Repository()
        iterator = iter(data)
        repository.id = next(iterator)
        repository.owner_id = next(iterator)
        repository.name = next(iterator)
        repository.description = next(iterator)

        repository.num_issues = next(iterator)

        repository.is_private = next(iterator)

        repository.created_at = next(iterator)
        repository.updated_at = next(iterator)

        return repository

    @staticmethod
    def get_repository_by_name_and_owner(
            owner_id: int,
            name: str
            ) -> Optional['Repository']:
        select_query = '''
        SELECT * FROM repository WHERE owner_id = %s AND name = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (owner_id, name))
            repo_data = cursor.fetchone()
            return Repository.from_db_row(repo_data)
                if repo_data is not None else None

    @staticmethod
    def get_accessible_for_user(user_id: int) -> List[int]:
        select_query = '''
        SELECT repo_id FROM repo_access WHERE user_id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (user_id,))
            db_rows = cursor.fetchall()
            return [db_row[0] for db_row in db_rows]

    @staticmethod
    def get_by_id(repo_id: int) -> Optional['Repository']:
        select_query = '''
        SELECT * FROM repository WHERE id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (repo_id,))
            repo_data = cursor.fetchone()
            return Repository.from_db_row(repo_data) 
                if repo_data is not None else None

    @staticmethod
    def is_repo_name_occupied(repo_name: str, user_id: int):
        select_query = '''
        SELECT id FROM repository WHERE name = %s AND owner_id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (repo_name, user_id))
            return cursor.fetchone() is not None

    @staticmethod
    def get_all(user_id: int) -> List['Repository']:
        select_query = '''
        SELECT DISTINCT ON (repository.id) repository.* FROM repository
            JOIN repo_access ra on repository.id = ra.repo_id
            WHERE ra.user_id = %(user)s OR repository.is_private = false;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                select_query,
                {'user': user_id}
            )
            db_rows = cursor.fetchall()
        return [Repository(**db_row) for db_row in db_rows]

    @staticmethod
    def get_by_user_id(user_id: int) -> List['Repository']:
        select_query = '''
        SELECT * FROM repository
            JOIN repo_access ra on repository.id = ra.repo_id
            WHERE ra.user_id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (user_id,))
            db_rows = cursor.fetchall()
            repos = [
                Repository.from_db_row(db_row)
                for db_row in db_rows
            ]
            for repo in repos:
                repo.owner = User.get_by_id(repo.owner_id)
            return repos
