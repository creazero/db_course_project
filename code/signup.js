const validatePass = (rule, value, callback) => {
    if (this.form.repeatPassword !== "") {
        this.$refs.form.validateField("repeatPassword");
    }
    callback();
};
const validatePass2 = (rule, value, callback) => {
    if (value !== this.form.password) {
        callback(new Error("Пароли не совпадают"));
    } else {
        callback();
    }
};

return {
    username: "",
    email: "",
    password: "",
    repeatPassword: "",
    isLoading: false,
    nonMatchingPasswords: false,

    form: {
        username: "",
        email: "",
        password: "",
        repeatPassword: ""
    },
    formRules: {
        username: [
            {
                required: true,
                message: "Обязательное поле",
                trigger: "blur"
            },
            {
                min: 5,
                max: 25,
                message: "Длина должна быть от 5 до 25 символов",
                trigger: "blur"
            }
        ],
        email: [
            {
                required: true,
                message: "Обязательное поле",
                trigger: "blur"
            },
            {
                type: "email",
                message: "Неверный формат",
                trigger: "blur"
            }
        ],
        password: [
            {
                required: true,
                message: "Обязательное поле",
                trigger: "blur"
            },
            {
                min: 6,
                message: "Длина должна быть от 6 символов",
                trigger: "blur"
            },
            { validator: validatePass, trigger: "blur" }
        ],
        repeatPassword: [
            {
                required: true,
                message: "Обязательное поле",
                trigger: "blur"
            },
            {
                min: 6,
                message: "Длина должна быть от 6 символов",
                trigger: "blur"
            },
            { validator: validatePass2, trigger: "blur" }
        ]
    }
};
