class User:
    def __init__(self, **kwargs):
        self.id = None
        self.username = None
        self.full_name = None
        self.email = None
        self.password = None
        self.num_repos = None
        self.created_at = None
        self.updated_at = None
        self.__parse_kwargs(**kwargs)

        self.token = None
        self.accessible_repos = []

    def check_password(self, password: str) -> bool:
        return bcrypt.checkpw(password.encode('utf-8'),
                            self.password.encode('utf-8'))

    def merge(self, patch_data: dict) -> 'User':
        if 'full_name' in patch_data:
            self.full_name = patch_data['full_name']

        if 'password' in patch_data:
            self.password = patch_data['password']

        if 'email' in patch_data:
            self.email = patch_data['email']

        return self

    def save(self, commit=False) -> None:
        insert_query = """
        INSERT INTO t_user (username, email, full_name, password)
        VALUES (%(username)s, %(email)s, %(full_name)s, %(password)s)
        RETURNING *;
        """
        update_query = '''
        UPDATE t_user
            SET email = %(email)s,
                full_name = %(full_name)s,
                password = %(password)s
            WHERE id = %(id)s;
        '''
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            if self.id is None:
                cursor.execute(
                    insert_query,
                    {
                        'username': self.username,
                        'email': self.email,
                        'password': self.password,
                        'full_name': self.full_name
                    }
                )
                db_row = cursor.fetchone()
                self.__parse_kwargs(**db_row)
            else:
                cursor.execute(
                    update_query,
                    {
                        'email': self.email,
                        'full_name': self.full_name,
                        'password': self.password,
                        'id': self.id
                    }
                )
            if commit:
                g.db.commit()

    def __parse_kwargs(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.username = kwargs.get('username', None)
        self.full_name = kwargs.get('full_name', None)
        self.email = kwargs.get('email', None)
        self.password = kwargs.get('password', None)
        self.num_repos = kwargs.get('num_repos', None)
        self.created_at = kwargs.get('created_at', None)
        self.updated_at = kwargs.get('updated_at', None)

    @staticmethod
    def hash_password(password: str) -> str:
        encrypted_password = bcrypt.hashpw(
            password.encode('utf-8'),
            bcrypt.gensalt()
        )
        return encrypted_password.decode('utf-8')

    @staticmethod
    def get_all() -> List['User']:
        select_query = 'SELECT * FROM t_user'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(select_query)
            db_rows = cursor.fetchall()
            return [User(**db_row) for db_row in db_rows]

    @staticmethod
    def get_by_id(user_id: int) -> Optional['User']:
        query = 'SELECT * FROM t_user WHERE id = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, (user_id,))
            user_data = cursor.fetchone()
            return User(**user_data) if user_data is not None else None

    @staticmethod
    def get_by_email(email: str) -> Optional['User']:
        query = 'SELECT * FROM t_user WHERE email = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, (email,))
            user_data = cursor.fetchone()
            return User(**user_data) if user_data is not None else None

    @staticmethod
    def get_by_username(username: str) -> Optional['User']:
        query = 'SELECT * FROM t_user WHERE username = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, (username,))
            user_data = cursor.fetchone()
            return User(**user_data) if user_data is not None else None

    @staticmethod
    def is_credentials_unique(username: str, email: str) -> bool:
        query = 'SELECT * FROM t_user WHERE username = %s OR email = %s;'
        with g.db.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, (username, email))
            return cursor.fetchone() is None
