class Label:
    def __init__(self, **kwargs):
        self.id = kwargs.get('id', None)
        self.title = kwargs.get('title', None)
        self.description = kwargs.get('description', None)
        self.color = kwargs.get('color', None)
        self.repo_id = kwargs.get('repo_id', None)

    def delete(self) -> None:
        query = 'DELETE FROM label WHERE id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(query, (self.id,))

    def get_issues_count(self) -> int:
        select_query = '''
        SELECT COUNT(*) FROM issue_tag
            WHERE label_id = %s;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (self.id,))
            return cursor.fetchone()[0]

    def merge(self, label_data: dict) -> 'Label':
        if 'color' in label_data:
            self.color = label_data.get('color')
        if 'description' in label_data:
            self.description = label_data.get('description')
        if 'title' in label_data:
            self.title = label_data.get('title')
        return self

    def save(self, commit=True) -> None:
        update_query = '''
        UPDATE label
            SET title = %s,
                description = %s,
                color = %s
            WHERE id = %s;
        '''
        insert_query = '''
        INSERT INTO label (title, description, color, repo_id)
            VALUES (%s, %s, %s, %s)
            RETURNING id;
        '''
        with g.db.cursor() as cursor:
            if self.id is not None:
                cursor.execute(
                    update_query,
                    (self.title, self.description,
                        self.color, self.id)
                )
            else:
                cursor.execute(
                    insert_query,
                    (self.title, self.description,
                        self.color, self.repo_id)
                )
                self.id = cursor.fetchone()[0]
            if commit:
                g.db.commit()

    @staticmethod
    def from_db_row(db_row: tuple) -> 'Label':
        iterator = iter(db_row)
        label = Label()
        label.id = next(iterator)
        label.title = next(iterator)
        label.description = next(iterator)
        label.color = next(iterator)
        label.repo_id = next(iterator)

        return label

    @staticmethod
    def get_by_id(label_id: int) -> Optional['Label']:
        select_query = 'SELECT * FROM label WHERE id = %s;'
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (label_id,))
            db_row = cursor.fetchone()
        return Label.from_db_row(db_row)
            if db_row is not None else None

    @staticmethod
    def get_from_repo(repo_id: int) -> List['Label']:
        select_query = '''
        SELECT * FROM label
            WHERE repo_id = %s ORDER BY label.title;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (repo_id,))
            db_rows = cursor.fetchall()
            return [
                Label.from_db_row(db_row)
                for db_row in db_rows
            ] if db_rows is not None else []

    @staticmethod
    def get_from_issue(issue_id: int) -> List['Label']:
        select_query = '''
        SELECT * FROM label
            JOIN issue_tag it on label.id = it.label_id
            WHERE it.issue_id = %s
            ORDER BY title;
        '''
        with g.db.cursor() as cursor:
            cursor.execute(select_query, (issue_id,))
            db_rows = cursor.fetchall()
        return [Label.from_db_row(db_row) for db_row in db_rows]
