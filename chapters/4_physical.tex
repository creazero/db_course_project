\section{Физическое проектирование}
Физическое проектирование базы данных --- процесс подготовки описания реализации
базы данных; на этом этапе рассматриваются основные отношения, организация
файлов и индексов, предназначенных для обеспечения эффективного доступа к
данным, а также все связанные с этим ограничения целостности и средства защиты \cite{project}.

Физическое проектирование является третьим и последним этапом создания проекта
базы данных, при выполнении которого проектировщик принимает решения о способах
реализации разрабатываемой базы данных. Во время предыдущего этапа
проектирования была определена логическая структура базы данных (которая
описывает отношения и ограничения в рассматриваемой прикладной области). Хотя
эта структура не зависит от конкретной целевой СУБД, она создается с учетом
выбранной модели хранения данных, например реляционной, сетевой или
иерархической. Однако, приступая к физическому проектированию базы данных,
прежде всего необходимо выбрать конкретную целевую СУБД. Поэтому физическое
проектирование неразрывно связано с конкретной СУБД. Между логическим и
физическим проектированием существует постоянная обратная связь, так как
решения, принимаемые на этапе физического проектирования с целью повышения
производительности системы, способны повлиять на структуру логической модели
данных.

Логическая модель данных, составленная на предыдущем этапе, является источником
информации об отношениях для физического проектирования. С её помощью
разработчик физической модели данных может изучить все аспекты использования
данных и выбрать наиболее подходящее решение в рамках предметной области
базы данных.

На текущий момент в мире существует огромное множество разных СУБД, но для
данной работы была выбрана СУБД PostgreSQL из-за того, что она распространяется
под свободной лицензией \cite{postgres}.

PostgreSQL базируется на языке SQL и поддерживает многие из возможностей
стандарта SQL:2011 \cite{iso-sql-2011}.

Сильными сторонами PostgreSQL считаются:
\begin{itemize}
	\item высокопроизводительные и надежные механизмы транзакций и репликации;
	\item расширяемая система встроенных языков программирования: в стандартной
		поставке поддерживаются PL/pgSQL, PL/Perl, PL/Python и\\PL/Tcl;
	\item наследование;
	\item возможность индексирования геометрических объектов и наличие
		базирующегося на ней расширения PostGIS;
	\item встроенная поддержка слабоструктурированных данных в формате\\JSON с
		возможностью их индексации;
	\item расширяемость (возможность создавать новые типы данных, типы индексов,
		языки программирования, модули расширения, подключать любые внешние
		источники данных).
\end{itemize}

Для создания базы данных необходимо составить структуры таблиц
(см. таблицы 1-8), основанных на сведениях о сформированном наборе схем
отношений между сущностями, созданных на этапе логического проектирования базы
данных. Полная схема данных отображена на рис. \ref{data-structure}.

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<ДоступКРепозиторию>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 	& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	user\_id 		& integer	 	& NOT NULL 		& Нет 	& - 	& - 		& Пользователь.ID \\
	\hline
	repo\_id 		& integer	 	& NOT NULL 		& Нет 	& - 	& - 		& Репозиторий.ID \\
	\hline
\end{xltabular}
}

\newpage

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Пользователь>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 	& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	username 		& varchar(50) 	& NOT NULL 		& Нет 	& - 	& UNIQUE, length > 5 	& - \\
	\hline
	full\_name 		& varchar(100) 	& - 			& Нет	& - 	& - 		& - \\
	\hline
	email	 		& varchar(100) 	& NOT NULL		& Нет	& - 	& UNIQUE, LIKE '\%@\%'	& - \\
	\hline
	password 		& varchar(150) 	& NOT NULL		& Нет	& - 	& - 		& - \\
	\hline
	created\_at		& timestamp 	& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
	updated\_at		& timestamp	 	& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
	num\_repos	 	& integer		& -				& Нет	& 0		& - 		& - \\
	\hline
\end{xltabular}
}

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Репозиторий>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 	& NOT NULL 		& Да 	& - 		& - 		& - \\
	\hline
	owner\_id 		& integer	 	& NOT NULL 		& Нет 	& - 		& - 		& Пользователь.ID \\
	\hline
	name	 		& varchar(50) 	& NOT NULL 		& Нет 	& - 		& - 		& - \\
	\hline
	num\_issues		& integer	 	& NOT NULL 		& Нет 	& 0			& - 		& - \\
	\hline
	is\_private		& bool		 	& NOT NULL 		& Нет 	& false		& - 		& - \\
	\hline
	created\_at		& timestamp 	& NOT NULL		& Нет	& NOW 		& - 		& - \\
	\hline
	updated\_at		& timestamp	 	& NOT NULL		& Нет	& NOW		& - 		& - \\
	\hline
\end{xltabular}
}

\newpage

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Задача>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 		& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	author\_id 		& integer	 		& NOT NULL 		& Нет 	& - 	& - 		& Пользователь.ID \\
	\hline
	repo\_id 		& integer	 		& NOT NULL 		& Нет 	& - 	& - 		& Репозиторий.ID \\
	\hline
	title	 		& varchar(100) 		& NOT NULL 		& Нет 	& - 	& - 		& - \\
	\hline
	content	 		& varchar(10000)	& -		 		& Нет 	& - 	& - 		& - \\
	\hline
	assignee\_id	& integer	 		& - 			& Нет 	& - 	& - 		& Пользователь.ID \\
	\hline
	is\_closed		& bool		 		& NOT NULL		& Нет 	& false	& - 		& - \\
	\hline
	milestone\_id 	& integer	 		& - 			& Нет 	& - 	& - 		& Этап.ID \\
	\hline
	num\_comments 	& integer	 		& NOT NULL		& Нет 	& 0 	& - 		& - \\
	\hline
	created\_at		& timestamp 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
	updated\_at		& timestamp	 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
\end{xltabular}
}

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<ТегЗадачи>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 	& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	label\_id 		& integer	 	& NOT NULL 		& Нет 	& - 	& - 		& Тег.ID \\
	\hline
	issue\_id 		& integer	 	& NOT NULL 		& Нет 	& - 	& - 		& Задача.ID \\
	\hline
\end{xltabular}
}

\newpage

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Тег>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 		& NOT NULL 		& Да 	& - 		& - 		& - \\
	\hline
	title			& varchar(20)		& NOT NULL 		& Нет 	& - 		& - 		& - \\
	\hline
	description		& varchar(1000)		& -		 		& Нет 	& - 		& - 		& - \\
	\hline
	color			& varchar(7)		& NOT NULL		& Нет 	& \#999999 	& length = 7	& - \\
	\hline
	repo\_id		& integer			& NOT NULL 		& Нет 	& - 		& - 		& Репозиторий.ID \\
	\hline
\end{xltabular}
}

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Этап>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 		& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	repo\_id 		& integer	 		& NOT NULL 		& Нет 	& - 	& - 		& Репозиторий.ID \\
	\hline
	title	 		& varchar(100) 		& NOT NULL 		& Нет 	& - 	& - 		& - \\
	\hline
	description		& varchar(1000)		& -		 		& Нет 	& - 	& - 		& - \\
	\hline
	num\_issues 	& integer	 		& NOT NULL		& Нет 	& 0 	& - 		& - \\
	\hline
	created\_at		& timestamp 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
	updated\_at		& timestamp	 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
\end{xltabular}
}

\newpage

{\small
\begin{xltabular}{\linewidth}{|c|c|C|c|C|C|c|}
	\caption{\newline Структура таблицы <<Комментарий>>}\\
	\hline
	\textbf{Столбец} & \textbf{Тип данных} & \textbf{Нуль} & \textbf{Ключ}
	& \textbf{Значе\-ние по умолчанию} & \textbf{Огра\-ни\-чение} & \textbf{Ссылка}\\
	\hline
	ID 				& integer	 		& NOT NULL 		& Да 	& - 	& - 		& - \\
	\hline
	type 			& integer	 		& NOT NULL 		& Нет 	& 0 	& >=0, <15	& - \\
	\hline
	author\_id	 	& integer			& NOT NULL 		& Нет 	& - 	& - 		& Пользователь.ID \\
	\hline
	issue\_id	 	& integer			& NOT NULL 		& Нет 	& - 	& - 		& Задача.ID \\
	\hline
	repo\_ID		& integer			& NOT NULL 		& Нет 	& - 	& - 		& Репозиторий.ID \\
	\hline
	content			& varchar(10000)	& -		 		& Нет 	& - 	& - 		& - \\
	\hline
	created\_at		& timestamp 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
	updated\_at		& timestamp	 		& NOT NULL		& Нет	& NOW 	& - 		& - \\
	\hline
\end{xltabular}
}

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{img/public-new.png}
	\caption{Схема таблиц базы данных}
	\label{data-structure}
\end{figure}

В Приложении Б приведены примеры заполнения вышеперечисленных таблиц.

Также для ускорения работы базы данных и упрощения кода в приложении, были созданы триггеры и индексы. Рассмотрим созданные объекты базы данных.

В приложении А (п. 1) представлен код триггера для таблицы <<Репозиторий>>. Данный триггер типа AFTER выполняется на любом изменении (вставки, изменения, удаления) таблицы <<Репозиторий>> и изменяет счетчик num\_repos для владельца репозитория.

В приложении А (п. 2) представлен код триггера для таблицы <<Репозиторий>>, который срабатывает на добавлении новых строк в таблицу. Требуется для того, чтобы для каждого нового репозитория создавать запись в таблице <<ДоступКРепозиторию>>. Это упростит работу с доступными для пользователя репозиториями.

В приложении А (п. 3) представлен код триггера для таблицы <<Задача>>, который срабатывает после добавления, изменения или удаления строк в таблице. Используется для того, чтобы изменять счетчик в таблице <<Репозиторий>>.

В приложении А (п. 4) представлен код триггера для таблицы <<Задача>>, который срабатывает после добавления или удаления строк, а также при изменении столбца milestone\_id. С помощью данного триггера изменяется значение счетчика num\_issues в таблице <<Этап>>.

В приложении А (п. 5) представлен код триггера для таблицы <<Комментарий>>, который срабатывает после добавления или удаления строк из таблицы. Используется для того, чтобы изменять значение счетчика num\_comments в таблице <<Задача>>.

В приложении А (п. 6-8) представлены коды для создания индексов для таблиц <<Задача>>, <<Тег>> и <<Комментарий>>. Данные индексы требуются для того, чтобы ускорить поиск по данным таблицам.